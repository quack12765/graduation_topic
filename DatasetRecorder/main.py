import cv2
import mediapipe as mp
import time
import os 
import numpy as np

#   [[      Set custom option      ]] 

FolderPath      = os.path.join("DatasetRecorder", "Themes")
ThemeName       = "鹹"                                      # set the name of the recording theme
KeyFrame        = "1"      
sec             = 20                                        # set the time for each recording of the theme
RecordTimes     = 1                                         # set the record times of you need   
fps             = 30                                        # set the fps of the output video
TeamMemberIndex = 0                                         # ( lun: 0, yu: 1, lan: 2, lee: 3 ) 

#   [[      Initialize      ]]

cap         = cv2.VideoCapture(0, cv2.CAP_DSHOW)
mp_drawing  = mp.solutions.drawing_utils
mp_holistic = mp.solutions.holistic
holistic    = mp_holistic.Holistic()

if not os.path.isdir(os.path.join(FolderPath, ThemeName)) :
    print(os.path.join(FolderPath, ThemeName))
    os.makedirs(os.path.join(FolderPath, ThemeName))

if not os.path.isdir(os.path.join(FolderPath, ThemeName, KeyFrame)) :
    os.mkdir(os.path.join(FolderPath, ThemeName, KeyFrame))

#   [[      Getting video size      ]]

size = None
while True :
    print("Getting Size, please enable your camera.")
    isGrep, img = cap.read()
    if isGrep :
        size = (img.shape[1], img.shape[0])
        break
print("video size: %d X %d" %(size[0], size[1]))

#   [[      Initialize some files       ]]

MDB = np.empty((size[1], size[0], 3), dtype='uint8')
MDB.fill(0)
cv2.imwrite("Mediapipe_Drawing_BackGround.jpg", MDB)

#   [[      Setting video writter       ]]
VIDfourcc = cv2.VideoWriter_fourcc(*"XVID")
MP4fourcc = cv2.VideoWriter_fourcc(*"mp4v")
# MP4videoWritter_MDB  = cv2.VideoWriter("%s/MDB.mp4" % ThemeName, MP4fourcc, fps, size)

cv2.putText(img, f'Ready', (20, 70), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (255, 0, 0))
cv2.imshow("", MDB)
cv2.waitKey(1500)

count = 0
while count < RecordTimes :
    count += 1
    MP4videoWritter_DATA = cv2.VideoWriter("%s/%s/%s/%d.mp4" % (FolderPath, ThemeName, KeyFrame, count + TeamMemberIndex * 12), MP4fourcc, fps, size)
    FrameCounts = 0
    Text = "0%"
    while True :
        isGrep, img = cap.read()
        if isGrep :
            data = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            result = holistic.process(data)
            
            if (result.left_hand_landmarks != None or result.right_hand_landmarks != None) and result.face_landmarks != None:
                # write the img to the videoWritter when at least one hand is detected
                MP4videoWritter_DATA.write(img)
                FrameCounts = FrameCounts + 1

            mp_drawing.draw_landmarks(img, result.face_landmarks, mp_holistic.FACEMESH_CONTOURS)
            mp_drawing.draw_landmarks(img, result.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS)
            mp_drawing.draw_landmarks(img, result.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS)
            
            cv2.putText(img, f'{int((FrameCounts / (fps * sec)) * 100)} %', (20, 70), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 0, 0))
            cv2.imshow("", img)

            cv2.waitKey(10)
            if FrameCounts == fps * sec :
                break
    

    tmpMDB = np.copy(MDB)
    

    if count == int(RecordTimes / 2) :
        Text = "!! Change hands !!"
    else :
        Text = f'%d / %d completed' %(count, RecordTimes)

    cv2.putText(tmpMDB, Text, (20, 70), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 255, 0))
    cv2.imshow("", tmpMDB)

    if count == int(RecordTimes / 2) :
        cv2.waitKey(2000)
    cv2.waitKey(1000)
    

cap.release()
cv2.destroyAllWindows()