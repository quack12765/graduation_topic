from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras.utils import to_categorical
import os
import numpy as np
import cv2
import mediapipe as mp
import sys

np.set_printoptions(suppress=True)

NumpyFolderUrl = os.path.join("./ExtractedKeyFrameIMG")
checkpoint_filepath = os.path.join("TrainingKeyFrameCheckpoint/checkpoint")
model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_filepath,
    # monitor="accuracy",
    save_weights_only=True,
    period=5
)

# Create label

FONTS = os.listdir(NumpyFolderUrl)

ALLKEYFRAME = []
for font in FONTS :
    for kf in os.listdir(os.path.join(NumpyFolderUrl, font)):
        ALLKEYFRAME.append(font + "#" + kf)

label_map = {label:num for num, label in enumerate(ALLKEYFRAME)}


from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense, Normalization, Flatten, ConvLSTM2D, MaxPooling2D, Conv2D
from tensorflow.keras.callbacks import TensorBoard
from transformers import TFAutoModel, AutoTokenizer
from keras_self_attention import SeqSelfAttention

log_dir = os.path.join('Logs')
tb_callback = TensorBoard(
    log_dir = log_dir, 
    histogram_freq = 5
)

model = Sequential()
model.add(Conv2D(filters=4, kernel_size=(5, 5), activation='relu', input_shape=(200, 200, 1)))
model.add(MaxPooling2D(pool_size=(3, 3)))
# model.add(Conv2D(filters=8, kernel_size=(5, 5), activation='relu'))
# model.add(MaxPooling2D(pool_size=(3, 3)))
model.add(Flatten())
model.add(Dense(len(ALLKEYFRAME), activation='softmax'))

model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['categorical_accuracy'])
model.summary()

model.load_weights(checkpoint_filepath)


# test

def draw_styled_landmarks(image, results):
    # Draw face connections
    mp_drawing.draw_landmarks(
        image, results.face_landmarks, mp_holistic.FACEMESH_CONTOURS, 
        mp_drawing.DrawingSpec(color=(80,110,10), thickness=1, circle_radius=1), 
        mp_drawing.DrawingSpec(color=(80,256,121), thickness=1, circle_radius=1)
    ) 

    # Draw left hand connections
    mp_drawing.draw_landmarks(
        image, results.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS, 
        mp_drawing.DrawingSpec(color=(255,255,255), thickness=1, circle_radius=1), 
        mp_drawing.DrawingSpec(color=(255,255,255), thickness=1, circle_radius=1)
    ) 
    # Draw right hand connections  
    mp_drawing.draw_landmarks(
        image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS, 
        mp_drawing.DrawingSpec(color=(255,255,255), thickness=1, circle_radius=1), 
        mp_drawing.DrawingSpec(color=(255,255,255), thickness=1, circle_radius=1)
    ) 

cap         = cv2.VideoCapture(0, cv2.CAP_DSHOW)
mp_drawing  = mp.solutions.drawing_utils
mp_holistic = mp.solutions.holistic
holistic    = mp_holistic.Holistic()


SEC = 5
FPS = 30

while True :

    times = 0

    while True :
        isGrep, img = cap.read()
        if isGrep :
            data = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            result = holistic.process(data)

            MDB = np.empty((200, 200, 3), dtype='uint8')
            MDB.fill(0)

            draw_styled_landmarks(MDB, result)

            if (result.left_hand_landmarks != None or result.right_hand_landmarks != None) and result.face_landmarks != None:
                
                PMDB = cv2.cvtColor(MDB, cv2.COLOR_RGB2GRAY).reshape(1, 200, 200, 1) / 255

                mapIndexToLabel = np.vectorize(lambda e : ALLKEYFRAME[e])

                res = model.predict(PMDB)
                res = np.where(res[0] > 0.5)
                res = mapIndexToLabel(res)

                print(res)

                times += 1

            if times == FPS * SEC :
                pass


        cv2.imshow("", MDB)
        cv2.waitKey(1000)




