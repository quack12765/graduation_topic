from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras.utils import to_categorical
import os
import numpy as np
import cv2
import mediapipe as mp
import sys

Mode = "_color"

np.set_printoptions(suppress=True)

NumpyFolderUrl = os.path.join("./ExtractedKeyFrameIMG" + Mode)
checkpoint_filepath = os.path.join("ResNet" + Mode + "/checkpoint")

# Create label

FONTS = os.listdir(NumpyFolderUrl)

ALLKEYFRAME = []
for font in FONTS :
    for kf in os.listdir(os.path.join(NumpyFolderUrl, font)):
        ALLKEYFRAME.append(font + "#" + kf)

label_map = {label:num for num, label in enumerate(ALLKEYFRAME)}

# print(ALLKEYFRAME)

from tensorflow.keras.applications import ResNet50

model = tf.keras.applications.ResNet50(include_top=True, weights=None, input_shape=(200, 200, 3), pooling=None, classes=len(ALLKEYFRAME))
model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['categorical_accuracy'])
model.load_weights(checkpoint_filepath)

# test

def draw_styled_landmarks(image, results):
    # Draw face connections
    mp_drawing.draw_landmarks(
        image, results.face_landmarks, mp_holistic.FACEMESH_CONTOURS, 
        mp_drawing.DrawingSpec(color=(80,110,10), thickness=1, circle_radius=1), 
        mp_drawing.DrawingSpec(color=(80,256,121), thickness=1, circle_radius=1)
    ) 

    if Mode == "_pose" :
        # Draw pose connections
        mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS,
                                mp_drawing.DrawingSpec(color=(80,22,10), thickness=1, circle_radius=1), 
                                mp_drawing.DrawingSpec(color=(80,44,121), thickness=1, circle_radius=1)
                                ) 

    # Draw left hand connections
    mp_drawing.draw_landmarks(
        image, results.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS, 
        mp_drawing.DrawingSpec(color=(255,255,0) if Mode == "_color" else (255,255,255), thickness=1, circle_radius=1), 
        mp_drawing.DrawingSpec(color=(255,255,0) if Mode == "_color" else (255,255,255), thickness=1, circle_radius=1)
    ) 
    # Draw right hand connections  
    mp_drawing.draw_landmarks(
        image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS, 
        mp_drawing.DrawingSpec(color=(0,0,255) if Mode == "_color" else (255,255,255), thickness=1, circle_radius=1), 
        mp_drawing.DrawingSpec(color=(0,0,255) if Mode == "_color" else (255,255,255), thickness=1, circle_radius=1)
    ) 

cap         = cv2.VideoCapture(0, cv2.CAP_DSHOW)
mp_drawing  = mp.solutions.drawing_utils
mp_holistic = mp.solutions.holistic
holistic    = mp_holistic.Holistic()


SEC = 5
FPS = 30

while True :

    times = 0

    while True :
        isGrep, img = cap.read()
        if isGrep :
            data = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            result = holistic.process(data)

            MDB = np.empty((200, 200, 3), dtype='uint8')
            MDB.fill(0)

            draw_styled_landmarks(MDB, result)

            PMDB = MDB

            if (result.left_hand_landmarks != None or result.right_hand_landmarks != None) and result.face_landmarks != None:
                
                if not Mode == "_color" :
                    PMDB = cv2.cvtColor(MDB, cv2.COLOR_RGB2GRAY)
                    PMDB = cv2.cvtColor(PMDB, cv2.COLOR_GRAY2BGR).reshape(1, 200, 200, 3) / 255
                else : 
                    PMDB = PMDB.reshape(1, 200, 200, 3) / 255

                mapIndexToLabel = np.vectorize(lambda e : ALLKEYFRAME[e])

                res = model.predict(PMDB)
                rs_index = np.where(res[0] > 0.8)
                
                if len(rs_index[0]) > 0 :

                    # do something while detected label 
                    score = res[0][rs_index[0][0]]
                    res = ALLKEYFRAME[rs_index[0][0]]

                    print(res, score)

                times += 1

            if times == FPS * SEC :
                pass


        cv2.imshow("", MDB)
        cv2.waitKey(1000)

            
