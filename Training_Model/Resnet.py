from asyncio import constants
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras.utils import to_categorical
import imgaug.augmenters as iaa
import os
import numpy as np
import matplotlib as plt
import mediapipe as mp
import sys
import cv2
import gc

# Create custom variable
Mode = "_color"
NumpyFolderUrl = os.path.join("./ExtractedKeyFrameIMG" + Mode)
checkpoint_filepath = os.path.join("ResNet" + Mode + "/checkpoint")

model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_filepath,
    # monitor="accuracy",
    save_weights_only=True,
    period=1
)
aug = iaa.SomeOf((0, None), [
    iaa.Affine(rotate=(-20, 20)),
    iaa.Affine(translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)}),
    iaa.Affine(scale=(0.5, 1.2)),
])


# Create label

FONTS = os.listdir(NumpyFolderUrl)

ALLKEYFRAME = []
for font in FONTS :
    for kf in os.listdir(os.path.join(NumpyFolderUrl, font)):
        ALLKEYFRAME.append(font + "#" + kf)

label_map = {label:num for num, label in enumerate(ALLKEYFRAME)}

# collect training set

file_list = []
DATASET_SIZE = 0

for font in FONTS :
    for kf in np.array(os.listdir(os.path.join(NumpyFolderUrl, font))):
        for fnpy in np.array(os.listdir(os.path.join(NumpyFolderUrl, font, kf))):
            # if int(set) < 13 :
            #     continue
        
            file_list.append(os.path.join(NumpyFolderUrl, font, kf, fnpy))
            DATASET_SIZE += 1


# initialize some module

mp_holistic = mp.solutions.holistic # Holistic model
mp_drawing = mp.solutions.drawing_utils # Drawing utilities
np.set_printoptions(suppress=True)
np.set_printoptions(threshold=sys.maxsize)


# Create some custom functions

def read_numpy(path):
    img = np.load(path.numpy(), mmap_mode="r")
    if not Mode == "_color" :
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    img = aug(image = img)
    return img / 255

def extract_label_from_path(path):
    res = np.zeros(len(ALLKEYFRAME))
    spath = path.numpy().decode('utf-8').split("\\")
    res[label_map[spath[-3] + "#" + spath[-2]]] = 1

    # print(label_map[spath[-3] + "#" + spath[-2]])
    return res


# build dataset


tf_dataset = tf.data.Dataset.from_tensor_slices(file_list).shuffle(len(file_list)).map(lambda item: [
    tf.py_function(read_numpy, inp=[item], Tout=[tf.float32,]), 
    tf.py_function(extract_label_from_path, inp=[item], Tout=[tf.int8,]), 
])

# for d in tf_dataset.take(10) :
#     cv2.imshow("", d[0][0].numpy())
#     cv2.waitKey(100)

# Splite train & test data

train_size = int(0.7 * DATASET_SIZE)
val_size = int(0.15 * DATASET_SIZE)
test_size = int(0.15 * DATASET_SIZE)

train_dataset = tf_dataset.take(train_size)
test_dataset = tf_dataset.skip(train_size)
val_dataset = test_dataset.skip(val_size)
test_dataset = test_dataset.take(test_size)

"""
    Build Model and fit

"""

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense, Normalization, Flatten, ConvLSTM2D, MaxPooling2D, Conv2D
from tensorflow.keras.callbacks import TensorBoard
from transformers import TFAutoModel, AutoTokenizer
from keras_self_attention import SeqSelfAttention
from tensorflow.keras.applications import ResNet50


log_dir = os.path.join('Logs')
tb_callback = TensorBoard(
    log_dir = log_dir, 
    histogram_freq = 5
)
 
model = tf.keras.applications.ResNet50(include_top=True, weights=None, input_shape=(200, 200, 3), pooling=None, classes=len(ALLKEYFRAME))
model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['categorical_accuracy'])
model.summary()

model.load_weights(checkpoint_filepath)

model.fit(
    train_dataset, 
    epochs=50, 
    validation_data=val_dataset,
    shuffle = True,
    callbacks=[
        # tb_callback,
        model_checkpoint_callback
    ],
)

# res = model.predict(test_dataset)

# print(res)