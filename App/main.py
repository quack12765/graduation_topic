from asyncio import constants
from multiprocessing.pool import MapResult
from tkinter import *
from tkinter.ttk import *
import time
import cv2
from PIL import Image,ImageTk 
from datetime import datetime
import numpy as np
import mediapipe as mp
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.applications import ResNet50
import os
import sys
import math

FramesCount = 60
Mode = "_color"

np.set_printoptions(suppress=True)

NumpyFolderUrl = os.path.join("./ExtractedKeyFrameIMG" + Mode)
checkpoint_filepath = os.path.join("ResNet" + Mode + "/checkpoint")

# Create label

FONTS = os.listdir(NumpyFolderUrl)

ALLKEYFRAME = []
WordSets = []
for font in FONTS :
    tset = set()
    for kf in os.listdir(os.path.join(NumpyFolderUrl, font)):
        l = font + "#" + kf
        ALLKEYFRAME.append(l)
        tset.add(l)
    WordSets.append({
        'label': font,
        'set': tset
    })

label_map = {label:num for num, label in enumerate(ALLKEYFRAME)}

mp_drawing  = mp.solutions.drawing_utils
mp_holistic = mp.solutions.holistic
holistic    = mp_holistic.Holistic()



model = tf.keras.applications.ResNet50(include_top=True, weights=None, input_shape=(200, 200, 3), pooling=None, classes=len(ALLKEYFRAME))
model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['categorical_accuracy'])
model.load_weights(checkpoint_filepath)

def draw_styled_landmarks(image, results):
    # Draw face connections
    mp_drawing.draw_landmarks(
        image, results.face_landmarks, mp_holistic.FACEMESH_CONTOURS, 
        mp_drawing.DrawingSpec(color=(80,110,10), thickness=1, circle_radius=1), 
        mp_drawing.DrawingSpec(color=(80,256,121), thickness=1, circle_radius=1)
    ) 

    if Mode == "_pose" :
        # Draw pose connections
        mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS,
                                mp_drawing.DrawingSpec(color=(80,22,10), thickness=1, circle_radius=1), 
                                mp_drawing.DrawingSpec(color=(80,44,121), thickness=1, circle_radius=1)
                                ) 

    # Draw left hand connections
    mp_drawing.draw_landmarks(
        image, results.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS, 
        mp_drawing.DrawingSpec(color=(255,255,0) if Mode == "_color" else (255,255,255), thickness=1, circle_radius=1), 
        mp_drawing.DrawingSpec(color=(255,255,0) if Mode == "_color" else (255,255,255), thickness=1, circle_radius=1)
    ) 
    # Draw right hand connections  
    mp_drawing.draw_landmarks(
        image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS, 
        mp_drawing.DrawingSpec(color=(0,0,255) if Mode == "_color" else (255,255,255), thickness=1, circle_radius=1), 
        mp_drawing.DrawingSpec(color=(0,0,255) if Mode == "_color" else (255,255,255), thickness=1, circle_radius=1)
    )

class Voter:

    def __init__(self):
        self.ans = None

    def buildAnsMap(self,ans):
        self.ans = ans

    def vote(self,test):
        
        array_point = [0] * len(self.ans)
        for i in range(0, len(test)):
            curWords = test[i]
            for j in range(0, len(curWords)):
                curWord = curWords[j]
                for k in range(0, len(self.ans)):
                    if len(self.ans[k]) == len(test) :
                        if self.ans[k][i] == curWord :
                            array_point[k] += 1

        max_value = 0
        max_loc = 0
        max_location = 0
        for num in array_point:
            if (num > max_value):
                max_value = num
                max_location =  max_loc
            max_loc =  max_loc + 1

        return max_location

VokeAns = []
MapAnsToSentence = []
with open("./App/word.txt", 'r', encoding='utf-8') as fh:
    VokeAns=[i[:-1].split(',') for i in fh.readlines()]

with open("./App/complete.txt", 'r', encoding='utf-8') as fh:
    MapAnsToSentence=[i[:-1] for i in fh.readlines()]


SentenceVoker = Voter()
SentenceVoker.buildAnsMap(VokeAns)

ActImg = []
isRunning = False

def detect_sentence():
    print("processing all images")
    FinalResult = []
    for i in range(0, len(ActImg)) :
        tset = set()
        MapLabelToCount = {}
        for j in range(0, len(ActImg[i])):
            frame = ActImg[i][j]

            data = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            result = holistic.process(data)

            MDB = np.empty((200, 200, 3), dtype='uint8')
            MDB.fill(0)

            draw_styled_landmarks(MDB, result)

            PMDB = MDB

            if (result.left_hand_landmarks != None or result.right_hand_landmarks != None) and result.face_landmarks != None:
                
                if not Mode == "_color" :
                    PMDB = cv2.cvtColor(MDB, cv2.COLOR_RGB2GRAY)
                    PMDB = cv2.cvtColor(PMDB, cv2.COLOR_GRAY2BGR).reshape(1, 200, 200, 3) / 255
                else : 
                    PMDB = PMDB.reshape(1, 200, 200, 3) / 255
                
                res = model.predict(PMDB)
                rs_index = np.where(res[0] > 0.8)
                
                if len(rs_index[0]) > 0 :

                    # do something while detected label 
                    res = ALLKEYFRAME[rs_index[0][0]]
                    MapLabelToCount[res] = MapLabelToCount[res] + 1 if res in MapLabelToCount else 1
                    tset.add(res)
  
        
        MapWordToScore = {}
        MapWordToCount = {}
        for k, v in MapLabelToCount.items() :
            k = k[:-2]
            # MapWordToCount[k] = MapWordToCount[k] + 1 if k in MapWordToCount else 1
            MapWordToScore[k] = (MapWordToScore[k] + v) * 1.5 if k in MapWordToScore else v

        total = 0
        countMapWordToScore = 0
        

        for k, v in MapWordToScore.items() :
            total += (v if v < 15 else 15)
            countMapWordToScore += 1

        avg = total / (countMapWordToScore + 1)
        possibleRes = []
        for k, v in MapWordToScore.items() :
            if v >= avg :
                possibleRes.append(k)

        print(MapLabelToCount)
        print(possibleRes)
        FinalResult.append(possibleRes)

    print('----------------------------')
    print(MapAnsToSentence[SentenceVoker.vote(FinalResult)])
    

def current_milli_time():
    return round(time.time() * 1000)

def running():
    global isRunning,ActImg
    if isRunning == True:
        pb["value"] = 0
        detect_sentence()
    isRunning = not isRunning
    btn.configure(text='output' if isRunning else 'run!')
    ActImg = []
    while isRunning:
        temp = []
        i = 0
        while i < FramesCount:
            i = i + 1
            # 進度條累加
            if isRunning == False:
                break
            
            root.update()

            #攝影機截圖
            _, pic = cam.read()
            frame = pic.copy()
            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            
            data = cv2.cvtColor(pic, cv2.COLOR_BGR2RGB)
            result = holistic.process(data)
            if (result.left_hand_landmarks != None or result.right_hand_landmarks != None) and result.face_landmarks != None:
                temp.append(pic)    
                pb["value"] = i * 100 / FramesCount 
            else:
                i = i - 1
            
            img = Image.fromarray(cv2image)
            imgtk = ImageTk.PhotoImage(image=img) #圖片對象轉換
            video.imgtk = imgtk
            video.configure(image=imgtk)
            
        ActImg.append(temp)
    

mp_holistic = mp.solutions.holistic
holistic    = mp_holistic.Holistic()
root = Tk()
pb = Progressbar(root,orient=HORIZONTAL,length=200,mode="determinate")
pb.pack(pady=20)
pb["maximum"] = 100
pb["value"] = 0


btn = Button(root,text="Running",command=running)
btn.pack(pady=10)

videoFrame = Frame(root).pack()
video = Label(videoFrame)
video.pack()
cam = cv2.VideoCapture(0)

root.mainloop()