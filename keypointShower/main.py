from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras.utils import to_categorical
import os
import numpy as np
import cv2
import mediapipe as mp
import sys

NumpyFolderUrl = os.path.join("./ExtractedKeyFrameIMG_color", '衛生紙', '1')


for kf in np.array(os.listdir(NumpyFolderUrl)):
    img = np.load(os.path.join(NumpyFolderUrl, kf), mmap_mode="r")
    # img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.imshow("", img)
    cv2.waitKey(50)